#  @Author : Vector
#  @Email  : vectorztt@163.com
#  @Time   : 2019/10/16 14:01
# ----------------------------------------
import json
import threading
import time
import numpy as np
import pandas as pd

from tencentcloud.common import credential
from tencentcloud.common.profile.client_profile import ClientProfile
from tencentcloud.common.profile.http_profile import HttpProfile
from tencentcloud.common.exception.tencent_cloud_sdk_exception import TencentCloudSDKException
from tencentcloud.asr.v20190614 import asr_client, models

from read_excel import get_word_info

SecretId = 'AKIDe4HF1YLuhkX7Ra8BdruhMmDOIvIPsK84'
SecretKey = 'kEVwJQfeNq5WcCeBTaQzIAl8rOefRUHQ'


def get_audio_result(word_info, word_list):
    try:
        cred = credential.Credential(SecretId, SecretKey)
        httpProfile = HttpProfile()
        httpProfile.endpoint = "asr.tencentcloudapi.com"

        clientProfile = ClientProfile()
        clientProfile.httpProfile = httpProfile
        client = asr_client.AsrClient(cred, "ap-beijing", clientProfile)

        req = models.CreateRecTaskRequest()
        params = '{"EngineModelType":"16k_0",' \
                 '"ChannelNum":1,' \
                 '"ResTextFormat":0,' \
                 '"SourceType":0,' \
                 '"Url":"%s"'\
                 '}' % word_info[2]
        req.from_json_string(params)

        resp = client.CreateRecTask(req)
        json_resp = json.loads(resp.to_json_string(), encoding='utf-8')
        task_id = json_resp['Data']['TaskId']
        result_req = models.DescribeTaskStatusRequest()
        result_params = '{"TaskId":%d}' % task_id
        result_req.from_json_string(result_params)
        time_count = 0
        while True:
            if time_count > 60:
                word_list.append([word_info[0], word_info[1], word_info[2], '单词时间等待过长'])
                print("单词时间等待过长", word_info[1], word_info[2], '\n')
                break
            result_resp = client.DescribeTaskStatus(result_req)
            result_json_resp = json.loads(result_resp.to_json_string(), encoding='utf-8')
            status = result_json_resp['Data']['StatusStr']
            result = result_json_resp['Data']['Result']
            if status != 'success':
                time.sleep(2)
                time_count += 2
            else:
                print(word_info[2])
                if result:
                    reform_result = result.split(']  ')[1].replace('\n', '')
                    if reform_result:
                        final_result = reform_result
                    else:
                        final_result = '识别结果为空'
                else:
                    final_result = "识别结果为空"
                print(word_info[1], final_result, '\n')
                word_list.append([word_info[0], word_info[1], word_info[2], final_result])
                break

    except TencentCloudSDKException as err:
        print(err)


def multi_process_get_result():
    word_info = get_word_info()
    word_list = []
    threading_list = []
    for i in range(len(word_info)):
        t = threading.Thread(target=get_audio_result, args=(word_info[i], word_list))
        threading_list.append(t)

    for x in range(len(threading_list)):
        threading_list[x].start()
        time.sleep(1)

    for x in range(len(threading_list)):
        threading_list[x].join()

    data = pd.DataFrame(np.array(word_list))
    writer = pd.ExcelWriter('tencent.xlsx')
    data.to_excel(writer, sheet_name='sheet1')
    writer.save()
    writer.close()


if __name__ == '__main__':
    multi_process_get_result()

