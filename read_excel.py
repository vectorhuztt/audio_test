#  @Author : Vector
#  @Email  : vectorztt@163.com
#  @Time   : 2019/10/16 14:03
# -----------------------------------------
import re
import pandas as pd


def get_word_info():
    word_info = []
    filename = '音频检测.xlsx'
    sheet = pd.read_excel(filename, sheet_name=1)
    index = sheet['index']
    words = sheet['key']
    audio_url = sheet['测试音频']
    for i in range(len(words)):
        word_index = index[i]
        word = words[i]
        url = audio_url[i]
        word_info.append((word_index, word, url))
    print('单词个数：', len(word_info))
    return word_info



