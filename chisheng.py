#  @Author : Vector
#  @Email  : vectorztt@163.com
#  @Time   : 2019/10/17 17:20
# -----------------------------------------
import hashlib
import json
import os
import time
import numpy as np
import pandas as pd
from urllib.request import Request, urlopen, urlretrieve

from websocket import create_connection

from read_excel import get_word_list


def get_audio_result(word_info, result_info):
    APP_KEY = "1571016306000001"
    SECRET_KEY = '8d760293a2198079e759cce3eec2b85d'
    wsurl = 'ws://cloud.chivox.com/ws?e=0&t=0&version=2'
    timestamp = int(round(time.time() * 1000))
    sig = hashlib.sha1((APP_KEY + str(timestamp) + SECRET_KEY).encode("utf8")).hexdigest()

    params = {
        "app":
            {"applicationId": APP_KEY,
             "sig": sig,
             "alg": "sha1",
             "timestamp": str(timestamp),
             "userId": "testAudio_Python"
             }
    }

    score_param = {
        "tokenId": "1234561234",
        "request": {
            "coreType": "en.word.score",
            "refText": word_info[1],
            "rank": 100,
            "userId": "UID",
            "attachAudioUrl": 1
        },
        "audio": {
            "audioType": "wav",
            "channel": 1,
            "sampleBytes": 2,
            "sampleRate": 16000
        }
    }

    ws = create_connection(wsurl)
    ws.send(json.dumps(params))
    ws.send(json.dumps(score_param))
    f = open('test.wav', 'rb')
    count = os.path.getsize('test.wav')
    while count > 0:
        if count < 1024:
            data = f.read(count)
            count = count - count
            ws.send_binary(data)
        else:
            data = f.read(1024)
            count = count - 1024
            ws.send_binary(data)
    ws.send_binary("")

    result = ws.recv()
    print(word_info[2])
    final_result = json.loads(result)
    try:
        audio_result = final_result['result']
    except KeyError:
        audio_result = None

    if audio_result:
        audio_score = int(audio_result['overall'])
        print("单词：", word_info[1],
              '音频得分：', audio_score, '\n')

        if 0 <= audio_score <= 54:
            score_content = ' (差)'
        elif 55 <= audio_score <= 69:
            score_content = " (中)"
        elif 70 <= audio_score <= 84:
            score_content = " (良)"
        else:
            score_content = " (优)"
        result_desc = str(audio_score) + score_content
    else:
        result_desc = '未识别出结果'

    result_info.append([word_info[0], word_info[1], word_info[2], result_desc])


if __name__ == '__main__':
    word_list = get_word_list()
    result_info = []
    try:
        for x in word_list:
            urlretrieve(x[2], 'test.wav')
            get_audio_result(x, result_info)
    except Exception as e:
        print(e)
        print('连接失败')
    finally:
        data = pd.DataFrame(np.array(result_info))
        writer = pd.ExcelWriter('excel/chisheng.xlsx')
        data.to_excel(writer, sheet_name='sheet1')
        writer.save()
        writer.close()