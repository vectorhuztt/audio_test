#  @Author : Vector
#  @Email  : vectorztt@163.com
#  @Time   : 2019/10/16 10:01
# -----------------------------------------
import base64
import hashlib
import json
import time
import requests
from urllib import request


def get_audio():
    request.urlretrieve('https://vanthink.oss-cn-qingdao.aliyuncs.com/eb5c5eab1e5e1367f2d5a9c0e9969f81.wav', '1.wav')


def main():
    x_appid = '5da58616'
    api_key = '49c1235537f09b2a7f34e5cfe91f2347'
    curTime = str(int(time.time()))
    url = 'http://api.xfyun.cn/v1/service/v1/ise'
    text = "[word]bird"
    AUDIO_PATH = r'3.wav'
    with open(AUDIO_PATH, 'rb') as f:
        file_content = f.read()
    base64_audio = base64.b64encode(file_content)
    body = {'audio': base64_audio, 'text': text}
    param = json.dumps(
        {"aue": "raw",
         "result_level": "simple",
         "language": "en_us",
         "category": "read_word"
         })
    paramBase64 = str(base64.b64encode(param.encode('utf-8')), 'utf-8')
    m2 = hashlib.md5()
    m2.update((api_key + curTime + paramBase64).encode('utf-8'))
    checkSum = m2.hexdigest()
    x_header = {
                'X-Appid': x_appid,
                'X-CurTime': curTime,
                'X-Param': paramBase64,
                'X-CheckSum': checkSum,
                'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
                }
    req = requests.post(url, data=body, headers=x_header)
    result = req.content.decode('utf-8')
    print(result)
    return


if __name__ == '__main__':
    # get_audio()
    main()
