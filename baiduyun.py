#  @Author : Vector
#  @Email  : vectorztt@163.com
#  @Time   : 2019/10/16 11:44
# -----------------------------------------
import json
import time

import numpy as np
import pandas as pd
from urllib.error import URLError
from urllib.parse import urlencode
from urllib.request import Request, urlopen, urlretrieve

from read_excel import get_word_info


class DemoError(Exception):
    pass


class BaiduAudioIdent:
    API_KEY = 'hUuhlliSa4pLmnw30QfGAhbQ'
    SECRET_KEY = 'yXrbboNwLZB0oqG2XGuuNPhsK9pakf34'
    AUDIO_FILE = '3.wav'
    FORMAT = AUDIO_FILE[-3:]
    CUID = '123456PYTHON'
    RATE = 16000
    DEV_PID = 1737
    ASR_URL = 'http://vop.baidu.com/server_api'
    SCOPE = 'audio_voice_assistant_get'
    TOKEN_URL = 'http://openapi.baidu.com/oauth/2.0/token'

    def get_token(self):
        params = {'grant_type': 'client_credentials',
                  'client_id': self.API_KEY,
                  'client_secret': self.SECRET_KEY
                  }
        post_data = urlencode(params).encode('utf-8')
        req = Request(self.TOKEN_URL, post_data)
        try:
            f = urlopen(req)
            result_str = f.read()
        except URLError as err:
            result_str = err.read()
        result = json.loads(result_str)
        if 'access_token' in result.keys() and 'scope' in result.keys():
            if self.SCOPE and (self.SCOPE not in result['scope'].split(' ')):  # SCOPE = False 忽略检查
                raise DemoError('scope is not correct')
            # print('SUCCESS WITH TOKEN: %s ; EXPIRES IN SECONDS: %s' % (result['access_token'], result['expires_in']))
            return result['access_token']
        else:
            raise DemoError('MAYBE API_KEY or SECRET_KEY not correct: access_token or scope not found in token response')

    def get_audio_result(self, token):
        with open(self.AUDIO_FILE, 'rb') as speech_file:
            speech_data = speech_file.read()
        length = len(speech_data)
        if length == 0:
            raise DemoError('file %s length read 0 bytes' % self.AUDIO_FILE)
        params = {'cuid': self.CUID, 'token': token, 'dev_pid': self.DEV_PID}
        params_query = urlencode(params)
        headers = {
            'Content-Type': 'audio/' + self.FORMAT + '; rate=' + str(self.RATE),
            'Content-Length': length
        }
        url = self.ASR_URL + "?" + params_query
        req = Request(self.ASR_URL + "?" + params_query, speech_data, headers)
        try:
            result_str = urlopen(req).read()
        except URLError as err:
            print('asr http response http code : ' + str(err.code))
            result_str = err.read()
        result_str = str(result_str, 'utf-8')
        json_result = json.loads(result_str)
        err_num = json_result['err_no']
        if err_num:
            if err_num == 3301:
                error_content = '音频质量过差'
            elif err_num == 3302:
                error_content = " 鉴权失败"
            elif err_num == 3303:
                error_content = "服务器后端繁忙 可能原始音频质量过差"
            elif err_num == 3304:
                error_content = "请求超限 请降低api请求评率"
            elif err_num == 3307:
                error_content = "语音服务后端识别出错， 可能原始音频质量过差"
            elif err_num == 3308:
                error_content = "音频过长, 不得超过60s"
            elif err_num == 3309:
                error_content = "音频数据问题 无法将音频转为pcm格式， 可能长度、格式问题"
            elif err_num == 3310:
                error_content = "音频时间过长"
            elif err_num == 3311:
                error_content = "采样率不为16K"
            elif err_num == 3314:
                error_content = "音频长度过短"
            elif err_num == 3316:
                error_content = "音频转为pcm 失败"
            else:
                error_content = err_num + '详情参照错误码列表'

            return error_content
        else:
            return json_result['result']

    # def get_final_result(self):
    #     token = self.get_token()
    #     self.get_audio_result(token)

    def audio_word_check_operate(self):
        result_info = []
        word_list = get_word_info()
        token = self.get_token()
        for x in word_list:
            word = x[0]
            word_audio_url = x[1]
            urlretrieve(word_audio_url, self.AUDIO_FILE)
            audio_result = self.get_audio_result(token)
            print(word, '  ', audio_result)
            result_info.append([word, word_audio_url, audio_result])
            time.sleep(1)
        data = pd.DataFrame(np.array(result_info))
        writer = pd.ExcelWriter('result.xlsx')
        data.to_excel(writer, sheet_name='sheet1')
        writer.save()
        writer.close()


if __name__ == '__main__':
    BaiduAudioIdent().audio_word_check_operate()
